package com.ef.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ef.domain.Access;
import com.ef.domain.LimitStatistic;

public interface AccessRepository extends JpaRepository<Access, Long>, JpaSpecificationExecutor<Access> {

	@Query("SELECT " + " new com.ef.domain.LimitStatistic(a.ip, COUNT(a)) " + //
			"FROM ACCESS a " + //
			"WHERE accessDate BETWEEN :fromDate AND :toDate " + //
			"GROUP BY ip " + //
			"HAVING COUNT(a) >= :treshold ")
	List<LimitStatistic> findLimitCount(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate,
			@Param("treshold") long treshold);

}
