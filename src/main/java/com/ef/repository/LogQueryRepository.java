package com.ef.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ef.domain.LogQuery;

public interface LogQueryRepository extends JpaRepository<LogQuery, Long>, JpaSpecificationExecutor<LogQuery> {

}
