package com.ef.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ef.domain.Access;
import com.ef.domain.LimitStatistic;
import com.ef.domain.LogQuery;
import com.ef.repository.AccessRepository;
import com.ef.repository.LogQueryRepository;

@Service
public class LogService {

	@Autowired
	private AccessRepository accessRepository;

	@Autowired
	private LogQueryRepository logQueryRepository;

	@PersistenceContext
	EntityManager entityManager;

	public long saveLog(String filename, String mode) {

		System.out.println("----- Start Upload Log -----");

		long lineCount = 0;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

		FileInputStream fstream;
		BufferedReader br = null;
		List<Access> accessList = new ArrayList<>();

		try {
			fstream = new FileInputStream(filename);
			br = new BufferedReader(new InputStreamReader(fstream));

			String strLine;

			while ((strLine = br.readLine()) != null) {

				int index = 0;
				String[] lineData = strLine.split("\\|");
				lineCount++;

				if (lineData.length >= 5) {
					Access access = new Access();

					try {
						access.setAccessDate(dateFormat.parse(lineData[index++]));
					} catch (Exception exception) {
						System.out.println("Line number " + lineCount + " has invalid date format");
					}

					access.setIp(lineData[index++]);
					access.setMethod(lineData[index++]);
					access.setResponse(lineData[index++]);
					access.setDetail(lineData[index]);
					accessList.add(access);

				} else {
					System.out.println("Line number " + lineCount + " has mismatch number of columns");
				}

			}

			accessRepository.deleteAll();
			accessRepository.saveAll(accessList);

			System.out.println("Upload Done");
			System.out.println("From file " + filename + " found " + lineCount + " records.");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return accessList.size();

	}

	public long findLimit(String date, String duration, String threshold) {

		List<LogQuery> logList = new ArrayList<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.hh:mm:ss");

		try {

			Date fromDate = dateFormat.parse(date);
			Calendar cal = Calendar.getInstance();
			cal.setTime(fromDate);

			if (duration.equalsIgnoreCase("hourly")) {
				cal.add(Calendar.HOUR, 1);
			} else if (duration.equalsIgnoreCase("daily")) {
				cal.add(Calendar.DATE, 1);
			}

			Date toDate = cal.getTime();

			if (NumberUtils.isParsable(threshold)) {

				Long thresholdValue = Long.valueOf(threshold);
				List<LimitStatistic> overLimitList = accessRepository.findLimitCount(fromDate, toDate, thresholdValue);

				for (LimitStatistic limitStatistic : overLimitList) {
					System.out.println(limitStatistic.getIp() + "  has " + limitStatistic.getCount()
							+ " requests between " + fromDate + " and " + toDate);
					LogQuery logQuery = new LogQuery();
					logQuery.setFromDate(fromDate);
					logQuery.setToDate(toDate);
					logQuery.setIp(limitStatistic.getIp());
					logQuery.setThreshold(thresholdValue);
					logQuery.setAmount(limitStatistic.getCount());
					logList.add(logQuery);
				}

				logQueryRepository.saveAll(logList);

			} else {
				System.out.println("Threshold should be number.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return logList.size();

	}

}
