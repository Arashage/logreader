package com.ef.domain;

public class LimitStatistic {

	private String ip;
	private long count;

	public LimitStatistic(String ip, long count) {
		this.ip = ip;
		this.count = count;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

}
