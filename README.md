# LogReader

#[Database]
- MySQL
- Username: root
- Password: root

** Schema and Database will generate auto


#[Pakage] (Build Jar)
```sh
mvn clean package
mvn clean package -Dmaven.test.skip=true (skip test)
```

#[Arguments]
```sh
--startDate
```
you can specific start date range here
default value "2017-01-01.00:00:00"

```sh
--duration
```
can specific between "hourly" and "daily"
default value "hourly"

```sh
--threshold 
```
can specific by number value

```sh
--accesslog
```
file name of log. You can specific file name or absolute path
default value "access.log" (file must put in same place with jar file)

```sh
--upload
```
upload status.
- "Y" mean delete all and upload value from file.
- "N" mean skip upload and use current value in database
- default value "Y"

# Test
```sh
java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100

----- Start Upload Log -----
Upload Done
From file access.log found 116484 records.
192.168.228.188  has 209 requests between Sun Jan 01 13:00:00 ICT 2017 and Sun Jan 01 14:00:00 ICT 2017
192.168.77.101  has 214 requests between Sun Jan 01 13:00:00 ICT 2017 and Sun Jan 01 14:00:00 ICT 2017
```

```sh
java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=daily --threshold=250 --upload=N

192.168.203.111  has 300 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.51.205  has 296 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.31.26  has 263 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.199.209  has 308 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.33.16  has 265 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.62.176  has 279 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.143.177  has 332 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.38.77  has 336 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.129.191  has 350 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.52.153  has 273 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.162.248  has 281 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
192.168.219.10  has 254 requests between Sun Jan 01 13:00:00 ICT 2017 and Mon Jan 02 13:00:00 ICT 2017
```

```sh
java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.15:00:00 --duration=hourly --threshold=200 --upload=N

192.168.106.134  has 232 requests between Sun Jan 01 15:00:00 ICT 2017 and Sun Jan 01 16:00:00 ICT 2017
192.168.11.231  has 211 requests between Sun Jan 01 15:00:00 ICT 2017 and Sun Jan 01 16:00:00 ICT 2017
```

```sh
java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500 --accesslog=d:\Git\temp\logreader\access.log

----- Start Upload Log -----
Upload Done
From file access.log found 116484 records.
192.168.31.26  has 591 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.62.176  has 582 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.219.10  has 533 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.162.248  has 623 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.199.209  has 640 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.185.164  has 528 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.52.153  has 541 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.102.136  has 513 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.129.191  has 747 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.203.111  has 601 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.38.77  has 743 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.33.16  has 584 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.206.141  has 536 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.51.205  has 610 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
192.168.143.177  has 729 requests between Sun Jan 01 00:00:00 ICT 2017 and Mon Jan 02 00:00:00 ICT 2017
```

# Remark
> If you cannot run check that you have local DB on change configuration and create new war file.

> Also check that your table not lock by liquibase if you stop/interupt process. You can drop schema and rerun.
